package mproxy

import (
	"errors"
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/backend"
	"github.com/emersion/go-imap/client"
	"github.com/jinzhu/gorm"
	"gitlab.com/ed13710/go-moderated-imap/model"
	"golang.org/x/crypto/bcrypt"
)

var (
	// ErrRecordNotFound returns a "record not found error". Occurs only when attempting to query the database with a struct; querying with a slice won't return this error
	ErrPasswordEncryptionFailure = errors.New("password encryption issue")
	ErrUserDoesNotExist          = errors.New("User does not exist")
	ErrMailboxDoesNotExist       = errors.New("User mailbox does not exist")
	ErrUserPasswordMismatch      = errors.New("User password does not match")
)

type Backend struct {
	DB *gorm.DB

	unexported struct{}
}

func New(db *gorm.DB) *Backend {
	return &Backend{
		DB: db,
	}
}

func (be *Backend) login(username, password string) (*client.Client, *model.ProxyMailbox, error) {
	var creds model.Credentials
	err := be.DB.Find(&creds, &model.Credentials{Username: username}).Error
	if err != nil {
		return nil, nil, ErrUserDoesNotExist
	}

	// Compare the stored hashed password, with the hashed version of the password that was received
	if err = bcrypt.CompareHashAndPassword([]byte(password), []byte(creds.Password)); err != nil {
		// If the two passwords don't match, return a 401 status
		return nil, nil, ErrUserPasswordMismatch
	}
	//client is authentificated
	var proxyMailbox model.ProxyMailbox

	err = be.DB.Find(&proxyMailbox, &model.ProxyMailbox{Username: username}).Error
	if err != nil {
		return nil, nil, ErrMailboxDoesNotExist
	}

	var c *client.Client
	if proxyMailbox.Security == model.SecurityTLS {
		tlsConfig := model.BuildTLSConfig(&proxyMailbox)
		if c, err = client.DialTLS(proxyMailbox.Address, tlsConfig); err != nil {
			return nil, nil, err
		}
	} else {
		if c, err = client.Dial(proxyMailbox.Address); err != nil {
			return nil, nil, err
		}

		if proxyMailbox.Security == model.SecuritySTARTTLS {
			tlsConfig := model.BuildTLSConfig(&proxyMailbox)
			if err := c.StartTLS(tlsConfig); err != nil {
				return nil, nil, err
			}
		}
	}

	if err := c.Login(username, password); err != nil {
		return nil, nil, err
	}

	return c, &proxyMailbox, nil
}

func (be *Backend) Login(connInfo *imap.ConnInfo, username, password string) (backend.User, error) {
	c, proxyMailbox, err := be.login(username, password)
	if err != nil {
		return nil, err
	}

	u := &user{
		be:      be,
		c:       c,
		mailbox: proxyMailbox,
	}
	return u, nil
}
