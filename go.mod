module gitlab.com/ed13710/go-moderated-imap-proxy

go 1.13

replace gitlab.com/ed13710/go-moderated-imap => ../go-moderated-imap

require (
	github.com/emersion/go-imap v1.0.4
	github.com/jinzhu/gorm v1.9.12
	gitlab.com/ed13710/go-moderated-imap v0.0.0-20200329165103-f1d245c1505c
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59
)
